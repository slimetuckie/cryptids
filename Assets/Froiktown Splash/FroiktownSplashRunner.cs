﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FroiktownSplashRunner : MonoBehaviour {
    public void OnEnable()
    {
        int targ = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(targ);
    }
}
