﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Personality {
    [SerializeField]
    public string[] dialogue;
    [SerializeField]
    public AudioClip[] voices;
    [SerializeField]
    public string name;
}

public class CryptidDialogue : MonoBehaviour {

    [SerializeField]
    public Personality[] types;
    public static Personality[] persons;

    public static Personality getPersonality(int index) {
        return (persons[index]);
    }

	// Use this for initialization
	void Awake () {
        persons = types;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
