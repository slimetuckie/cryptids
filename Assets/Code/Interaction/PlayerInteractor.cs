﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractor : MonoBehaviour {
    public float minInteractDist;

    InteractBase closest;

    float tempDist;
    float targetDist;
    Vector3 PlayerPos;
    public void checkClosest()
    {
        closest = null;
        targetDist = minInteractDist;
        PlayerPos = transform.position;
        foreach (InteractBase k in InteractBase.interactables)
        {
            if (k.CanInteractWith)
            {
                tempDist = Vector3.Distance(PlayerPos, k.transform.position);
                if (tempDist < targetDist)
                {
                    targetDist = tempDist;
                    closest = k;
                }
            }
        }
    }
    //presses A button to trigger this
    public void tryInteractWith()
    {
        if(closest != null)
        {
            StartCoroutine(closest.InteractWith());
        }
    }
}
