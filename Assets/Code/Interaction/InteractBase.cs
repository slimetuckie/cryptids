﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractBase : MonoBehaviour {
    public bool CanInteractWith = true;
    public static List<InteractBase> interactables = new List<InteractBase>();
    public virtual void OnEnable()
    {
        interactables.Add(this);
    }
    public virtual IEnumerator InteractWith()
    {

        yield return null;
    }
}
