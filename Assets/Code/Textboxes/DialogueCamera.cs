﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class DialogueCamera : MonoBehaviour {
    [SerializeField] CinemachineVirtualCamera vcam;

    public void setDialogueTarget(Transform t)
    {
        vcam.Follow = t;
        vcam.LookAt = t;
    }
    public void enableCamera(bool enable)
    {
        vcam.gameObject.SetActive(enable);
    }
}
