﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempNPC : InteractBase {
    public string characterName;

    int timesInteractedWith = 0;
    public string randomSaying = "Why are birds so angry at me?";
    public string catchPhrase = "Vehicular";
    public override IEnumerator InteractWith()
    {
        DialogueController.instance.DialogueCamera.setDialogueTarget(transform);
        List<string> t = new List<string>();
        if (timesInteractedWith % 4 == 0)
        {
            t.Add(catchPhrase);
        }
        else
        {
            t.Add(randomSaying);
        }
        DialogueController.instance.TryDoText(t, characterName, null);
        yield return null;
        timesInteractedWith++;
    }
}
