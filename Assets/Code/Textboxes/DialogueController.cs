﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueController : MonoBehaviour {
    public static DialogueController instance;
    TextWriter writer;
    DialogueAnimation anim;
    public AudioClip default_sound;
    public Transform nameObject;
    TMP_Text nameRenderer;
    public DialogueCamera DialogueCamera;

    public string inputButton = "Submit";

    public dialogueState State = dialogueState.none;

    public enum dialogueState
    {
        none,
        Talking
    }

    private void Awake()
    {
        instance = this;
        writer = GetComponent<TextWriter>();
        nameRenderer = nameObject.GetComponentInChildren<TMP_Text>();
        anim = GetComponent<DialogueAnimation>();
    }
    public void ClearText()
    {
    }

    public void TryDoText(List<string> text, string speakerName, Action onComplete, AudioClip[] voice = null) {
        if (voice == null) {
            voice = new AudioClip[] { default_sound };
        }
        if (writer.finished) {
            writer.finished = false;
            StartCoroutine(DoText(text, speakerName, onComplete, voice));
        }
    }

    public IEnumerator DoText(List<string> text, string speakerName, Action onComplete, AudioClip[] voice)
    {
        DialogueCamera.enableCamera(true);
        State = dialogueState.Talking;
        yield return null;
        nameRenderer.SetText(speakerName);
        yield return StartCoroutine(anim.MoveIn());
        //animate text in
        foreach (string t in text)
        {
            yield return StartCoroutine(writer.writeText(t, voice, null));
            //wait for input?
            yield return StartCoroutine(waitForInput());
        }
        yield return null;
        yield return StartCoroutine(anim.MoveOut());
        State = dialogueState.none;
        DialogueCamera.enableCamera(false);

        //Debug.Log(writer.textRenderer.text);
        writer.visibleText = "";
        //Debug.Log(writer..text);
    }

    IEnumerator waitForInput()
    {
        while(Input.GetButtonDown(inputButton) == false)
        {
            yield return null;
        }
    }
}
