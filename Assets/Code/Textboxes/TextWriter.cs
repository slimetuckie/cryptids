﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextWriter : MonoBehaviour {
    [SerializeField] float charactersPerSecond;
    [SerializeField] float spacePause;
    AudioSource audio;
    public Transform textObject;

    public TMP_Text textRenderer;
    public string visibleText;

    public bool finished = true;

    float letterPause()
    {
        return (1f / charactersPerSecond);
    }

    private void Start()
    {
        textRenderer = textObject.GetComponent<TMP_Text>();
        audio = GetComponent<AudioSource>();

    }

    private void Update()
    {
        textRenderer.SetText(visibleText);
    }    

    public IEnumerator writeText(string targetText, AudioClip[] voice, Action onComplete)
    {
        finished = false;
        textRenderer.SetText("");
        visibleText = "";
        for(int i =0; i < targetText.Length; i++)
        {
            visibleText += targetText[i];
            yield return new WaitForSeconds(letterPause());
            
            if(i % 3 == 0) {
                audio.clip = voice[UnityEngine.Random.Range(0, voice.Length - 1)];
                audio.Play();
            }
            
        }
        yield return null;

        if (onComplete != null) {
            
            onComplete();
        }
        finished = true;
        
    }
}
