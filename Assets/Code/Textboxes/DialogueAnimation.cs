﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueAnimation : MonoBehaviour {
    public AnimationCurve movementCurve;
    public MovingObjectClass top, bottom;

    public float transitionDur;

    [System.Serializable]
    public class MovingObjectClass
    {
        public RectTransform Object;
        public RectTransform inner, outer;

        public void applyData(float percent)
        {
            if (percent == 1)
            {
                Object.anchoredPosition = outer.anchoredPosition;
            }
            else
            {
                Object.anchoredPosition = Vector3.LerpUnclamped(inner.anchoredPosition, outer.anchoredPosition, percent);
            }
        }
    }

    public IEnumerator MoveIn()
    {
        var m = transitionDur + Time.time;
        float p;
        while (m > Time.time)
        {
            p = ((m - Time.time) / transitionDur);
            p = movementCurve.Evaluate(p);

            top.applyData(p);
            bottom.applyData(p);
            yield return null;
        }
        top.applyData(0);
        bottom.applyData(0);
    }

    public IEnumerator MoveOut()
    {
        var m = transitionDur + Time.time;
        float p;
        while (m > Time.time)
        {
            p = 1 - ((m - Time.time) / transitionDur);
            p = movementCurve.Evaluate(p);

            top.applyData(p);
            bottom.applyData(p);
            yield return null;
        }
        DialogueController.instance.ClearText();

        top.applyData(1);
        bottom.applyData(1);
        
    }
}
