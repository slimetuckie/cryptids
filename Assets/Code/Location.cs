﻿using System.Collections;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public class coords {
    public float lat, lng;
}

[System.Serializable]
public class LocationData {
    public coords location;
}

public class Location : MonoBehaviour {

    string url = "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyAOAGGljQm00TKR1_kf7aJHFwy5m3rR12c";
    Dictionary<string, string> conf = new Dictionary<string, string>();
    string json;

    public static Vector2 coords;

    // Use this for initialization
    void Start () {
        conf.Add("considerIp", "true");
        json = JsonUtility.ToJson(conf);
        StartCoroutine("webrequest");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator webrequest() {
        var req = new UnityWebRequest(url, "POST");
        byte[] bytes = Encoding.UTF8.GetBytes(json);
        req.SetRequestHeader("Content-Type", "application/json; charset=utf-8");
        req.uploadHandler = (UploadHandler)new UploadHandlerRaw(bytes);
        req.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        req.SetRequestHeader("Content-Type", "application/json");
        yield return req.SendWebRequest();
        LocationData data = JsonUtility.FromJson<LocationData>(req.downloadHandler.text);
        coords = new Vector2(data.location.lng, data.location.lat);
    }

}
