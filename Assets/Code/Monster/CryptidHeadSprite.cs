﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CryptidHeadSprite : CryptidPart {

    CryptidHeads sprites;
    Texture2D tex;

    float timeSinceChange;
    public override void CryptidUpdate(bool differentState)
    {
        if (differentState)
            timeSinceChange = Time.time;

        switch (logic.aState)
        {
            case CryptidNPCLogic.animState.Idle:
                sprite.sprite = sprites.idle;
                break;
            case CryptidNPCLogic.animState.Walking:
                sprite.sprite = sprites.walk[Mathf.FloorToInt((Time.time - timeSinceChange) * fps) % sprites.walk.Length];
                break;
            case CryptidNPCLogic.animState.Talking:
                sprite.sprite = sprites.walk[Mathf.FloorToInt((Time.time - timeSinceChange) * fps) % sprites.walk.Length];
                break;
        }
    }

    public override void FetchSprite()
    {
        sprite_index = logic.data.head_design;
        sprites = parts.heads[sprite_index];
//        tex = sprites.idle;
    }

    public override void Download() {
        sprite_index = logic.data.head_design;
        sprites = parts.heads[sprite_index];
//        tex = sprites.idle;
    }
}
