﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CryptidLegSprite : CryptidPart {

    CryptidLegs sprites;
    Texture2D tex;
    // Use this for initialization
    // Update is called once per frame

    public CryptidNPCLogic.animState STATE;
    float timeSinceChange;
    public int c = 0;
    public override void CryptidUpdate(bool differentState)
    {
        if (differentState)
            timeSinceChange = Time.time;

        STATE = logic.aState;
        switch (logic.aState)
        {
            case CryptidNPCLogic.animState.Idle:
                sprite.sprite = sprites.idle;
                break;
            case CryptidNPCLogic.animState.Walking:
             

                sprite.sprite = sprites.walk[Mathf.FloorToInt((Time.time - timeSinceChange) * fps) % sprites.walk.Length];
                break;
            case CryptidNPCLogic.animState.Talking:
                sprite.sprite = sprites.idle;
                break;
        }

        /*
        if (downloaded) {
            // sprite state machine that changes what tex is
            if (base_piece != null) {
                if (base_piece.texture != tex) {
                    base_piece = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
                    sprite.sprite = base_piece;
                }
                else
                {



                }
            }
            else if (downloaded) {
                base_piece = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
                sprite.sprite = base_piece;
            }
        }
        */
    }

    public override void FetchSprite()
    {
        sprite_index = logic.data.leg_design;
        sprites = parts.legs[sprite_index];
        sprite.sprite = sprites.idle;
 //       tex = sprites.idle;
    }

    public override void Download() {
        sprite_index = logic.data.leg_design;
        sprites = parts.legs[sprite_index];
        sprite.sprite = sprites.idle;
        //        tex = sprites.idle;
    }

}
