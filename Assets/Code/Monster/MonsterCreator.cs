﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterCreator : MonoBehaviour {
    public static CryptidData GlobalData = new CryptidData();

    [SerializeField] GameObject prefab;
    CryptidNPCLogic cryptid;
    [SerializeField]Transform refPos;

    public CryptidData data;
    public IntChanger Hat, Face, Head, Torso, Legs, Personality;

    public Material defaultMat;

    public static MonsterCreator instance;

    public TMPro.TMP_InputField nameTarg;
    public TMPro.TMP_InputField sayingTarg;



    private void Start()
    {
        instance = this;
        GenerateRandomMonster();
    }

    public void GenerateRandomMonster()
    {
        data = new CryptidData();

        data.body_design = Random.Range(0, 9);
        data.eye_design = Random.Range(0, 9);
        data.hat_design = Random.Range(0, 9);
        data.head_design = Random.Range(0, 9);
        data.leg_design = Random.Range(0, 9);

        data.personality = Random.Range(0, 6);

        Head.setInt(data.head_design);
        Hat.setInt(data.hat_design);
        Face.setInt(data.eye_design);
        Legs.setInt(data.leg_design);
        Torso.setInt(data.body_design);
        Personality.setInt(data.personality);

        data.catchphrase = "";
        data.catchphrase = "";


        if (cryptid == null)
        {
            cryptid = Instantiate(prefab).GetComponent<CryptidNPCLogic>();
            cryptid.transform.position = refPos.position;
            SpriteRenderer[] m = cryptid.GetComponentsInChildren<SpriteRenderer>();
            foreach(SpriteRenderer s in m)
            {
                s.material = defaultMat;
            }
        }
        updateMonsterData();
    }

    public void updateMonsterData()
    {
        data.head_design = Head.i;
        data.hat_design = Hat.i;
        data.eye_design = Face.i;
        data.leg_design = Legs.i;
        data.body_design = Torso.i;

        data.name = nameTarg.text;
        data.catchphrase = sayingTarg.text;

        data.personality = Personality.i;
        cryptid.doVisuals(data);
        GlobalData = data;
    }
}
