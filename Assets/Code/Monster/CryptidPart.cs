﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CryptidPart : MonoBehaviour {

    [SerializeField]
    public Sprite base_piece;

    [SerializeField]
    public int sprite_index = 0;

    public SpriteRenderer sprite;
    public CryptidNPCLogic logic;


    internal PartsList parts;
    
    internal float fps = 8;

    // Use this for initialization
    public virtual void Setup (CryptidNPCLogic parent) {
        parts = PartsList.instance;
        sprite = GetComponent<SpriteRenderer>();    
        logic = parent;
	}

    public virtual void CryptidUpdate(bool differentState)
    {

    }	
    virtual public void Download() {
        

    }
    public virtual void FetchSprite()
    {

    }
}
