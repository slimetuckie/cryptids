﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CryptidFaceSprite : CryptidPart {

    CryptidFaces sprites;
    Texture2D tex;
    // Use this for initialization

    // Update is called once per frame
    float timeSinceChange;
    public CryptidNPCLogic.animState STATE;
    public override void CryptidUpdate(bool differentState)
    {
        if (differentState)
            timeSinceChange = Time.time;
        STATE = logic.aState;
        switch (logic.aState)
        {
            case CryptidNPCLogic.animState.Idle:
                sprite.sprite = sprites.idle;
                break;
            case CryptidNPCLogic.animState.Walking:
                sprite.sprite = sprites.walk[Mathf.FloorToInt((Time.time - timeSinceChange) * fps) % sprites.walk.Length];
                break;
            case CryptidNPCLogic.animState.Talking:
                sprite.sprite = sprites.talk[Mathf.FloorToInt((Time.time - timeSinceChange) * fps) % sprites.talk.Length];
                break;
        }
    }

    public override void FetchSprite()
    {
        sprite_index = logic.data.eye_design;
        sprites = parts.faces[sprite_index];
        sprite.sprite = sprites.idle;
        //       tex = sprites.idle;

    }

    public override void Download() {
        sprite_index = logic.data.eye_design;
        sprites = parts.faces[sprite_index];
        sprite.sprite = sprites.idle;
        //       tex = sprites.idle;
    }

}
