﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CryptidHatSprite : CryptidPart {

    CryptidHats sprites;
    Texture2D tex;

    float timeSinceChange;
    public override void CryptidUpdate(bool differentState)
    {
        if (differentState)
            timeSinceChange = Time.time;

        switch (logic.aState)
        {
            case CryptidNPCLogic.animState.Idle:
                sprite.sprite = sprites.idle;
                break;
            case CryptidNPCLogic.animState.Walking:
                sprite.sprite = sprites.idle;
                break;
        }
    }

    public override void FetchSprite()
    {
        sprite_index = logic.data.hat_design;
        sprites = parts.hats[sprite_index];
        sprite.sprite = sprites.idle;
        //        tex = sprites.idle;

    }

    public override void Download() {
        sprite_index = logic.data.hat_design;
        sprites = parts.hats[sprite_index];
        sprite.sprite = sprites.idle;
        //        tex = sprites.idle;
    }

}
