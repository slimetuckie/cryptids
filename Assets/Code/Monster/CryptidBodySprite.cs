﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CryptidBodySprite : CryptidPart {

    CryptidBodys sprites;
    //Texture2D tex;
    float timeSinceChange;
    // Update is called once per frame
    public override void CryptidUpdate(bool differentState)
    {
        if (differentState)
            timeSinceChange = Time.time;

        switch (logic.aState)
        {
            case CryptidNPCLogic.animState.Idle:
                sprite.sprite = sprites.idle;
                break;
            case CryptidNPCLogic.animState.Walking:
                sprite.sprite = sprites.walk[Mathf.FloorToInt((Time.time - timeSinceChange) * fps) % sprites.walk.Length];
                break;
            case CryptidNPCLogic.animState.Talking:
                sprite.sprite = sprites.idle;
                break;
        }
    }

    public override void FetchSprite() {
        sprite_index = logic.data.body_design;
        sprites = parts.bodies[sprite_index];
        sprite.sprite = sprites.idle;
        //       tex = sprites.idle;
    }

    public override void Download() {
        sprite_index = logic.data.body_design;
        sprites = parts.bodies[sprite_index];
        sprite.sprite = sprites.idle;
        //        tex = sprites.idle;
    }

}
