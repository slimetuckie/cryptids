﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour {
    public Vector3 debugLocation;

    [SerializeField] GameObject CryptidRef;
    private void Start()
    {
        StartCoroutine(waitTillSpawn());
    }

    IEnumerator waitTillSpawn()
    {
        while (!Database.db.ready)
        {
            yield return null;
        }
        foreach(CryptidData cryptid in Database.db.cryptids.data) {
            SpawnCryptid(cryptid);
        }
        
    }

    public void SpawnCryptid(CryptidData dbInt)
    {
        CryptidNPCLogic cryptid = Instantiate(CryptidRef).GetComponent<CryptidNPCLogic>();
        Vector3 vec = new Vector3(UnityEngine.Random.Range(-0.075f, 0.075f), 0, UnityEngine.Random.Range(-0.075f, 0.075f));
        cryptid.transform.position = new Vector3 (dbInt.location_x * 0.2f, 0.01f, dbInt.location_y * 0.2f) + vec;

        cryptid.initializeData(dbInt);
    }

}
