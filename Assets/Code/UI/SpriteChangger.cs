﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteChangger : IntChanger
{
    public Image pImage;
    public Sprite[] Personalities;

    public override void setInt(int id)
    {
        i = id;
        pImage.sprite = Personalities[i];
    }

    public override void UpdateInt(int dir)
    {
        i += dir;
        i += Personalities.Length;
        i %= Personalities.Length;
        MonsterCreator.instance.updateMonsterData();
        pImage.sprite = Personalities[i];
    }
}
