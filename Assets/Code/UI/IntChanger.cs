﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntChanger : MonoBehaviour {
    [SerializeField]
    AudioClip clip;
    [SerializeField]
    AudioSource src;
    public int i;
    public int max = 9;
    public virtual void setInt(int id)
    {
        i = id;
    }
    public virtual void UpdateInt(int dir)
    {
        src.clip = clip;
        src.Play();
        i += dir;
        i += max;
        i %= max;
        MonsterCreator.instance.updateMonsterData();
    }
}
