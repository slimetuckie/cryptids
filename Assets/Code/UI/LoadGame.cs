﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGame : MonoBehaviour {


    public void LoadPlay()
    {

        CryptidData data = MonsterCreator.GlobalData;
        if(data.name != "" && data.catchphrase != "") {
            data.location_x = Location.coords.x;
            data.location_y = Location.coords.y;
            Database.UploadCryptid(data);
        }
        
        SceneManager.LoadScene(2);
    }
}
