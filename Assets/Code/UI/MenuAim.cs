﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuAim : MonoBehaviour {

    public Mover[] menuz;

    public float debugStart;

    [System.Serializable]
    public class Mover
    {
        public RectTransform obj;
        [SerializeField] RectTransform inner, outter;

        public void updatePos(float percent)
        {
            obj.anchoredPosition = Vector3.LerpUnclamped(outter.anchoredPosition, inner.anchoredPosition, percent);
        }
    }

    public bool isOpen;
    public float OpenSpeed;
    float pcent;
    float t;


    private void Update()
    {
        if (Input.GetMouseButtonDown(0)){
            isOpen = true;
        }

        t = isOpen ? 1 : 0;
        pcent = Mathf.MoveTowards(pcent, t, Time.deltaTime * OpenSpeed);


        setVariables(pcent);
    }

    [Range(0,1)]
    public float debug = 1;
    [ContextMenu("Update")]
    public void debugSet()
    {
        pcent = debug;
        setVariables(debug);
    }
    void setVariables(float value)
    {
        foreach(Mover m in menuz)
        {
            m.updatePos(value);
        }
    }
}
