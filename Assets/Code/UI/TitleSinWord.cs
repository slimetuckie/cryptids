﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleSinWord : MonoBehaviour {
    public RectTransform[] rcts;
    Vector3[] origins;

    public float magnituded;
    public float offset;
    public float period;

    private void Start()
    {
        origins = new Vector3[rcts.Length];
        for (int i = 0; i < origins.Length; i++)
        {
            origins[i] = rcts[i].anchoredPosition;
        }        
    }

    public void Update()
    {
        for (int i = 0; i < rcts.Length; i++)
        {
            rcts[i].anchoredPosition = origins[i] + Vector3.up * 
                Mathf.Sin((Time.time + (i * offset)) * period)
                * magnituded;
        }
    }
}
