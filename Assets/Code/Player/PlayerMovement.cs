﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;
    CharacterController controller;
    public bool moving;
    public void init() {
        controller = GetComponent<CharacterController>();
    }

    public void MoveUpdate() {
        if (controller.isGrounded) {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moving = moveDirection != Vector3.zero;

            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            
        }
        if (moveDirection != Vector3.zero) {
          //  moving = true;
            transform.localScale = new Vector3(Mathf.Cos(Time.fixedTime * 10) * 0.03f + 1, Mathf.Sin(Time.fixedTime * 10) * 0.05f + 1, 1);
        } else {
            //moving = false;
            transform.localScale = new Vector3(1, 1, 1);
        }

        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
        
    }
}
