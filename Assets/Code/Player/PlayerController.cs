﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : CryptidNPCLogic
{
    PlayerInteractor interactor;
    PlayerMovement mover;

    private void Start()
    {
        mover = GetComponent<PlayerMovement>();
        interactor = GetComponent<PlayerInteractor>();
        mover.init();
        doVisuals(MonsterCreator.instance.data);
    }

    public override void Update()
    {
        if (DialogueController.instance.State == DialogueController.dialogueState.Talking)
        {
            aState = animState.Talking;
        }
        switch (State)
        {
            case cryptidState.loaded:
                if (DialogueController.instance.State != DialogueController.dialogueState.Talking)
                {
                    interactor.checkClosest();
                    if (Input.GetButtonDown("Submit"))
                    {
                        interactor.tryInteractWith();
                        return;
                    }
                    mover.MoveUpdate();
                    aState = mover.moving ? animState.Walking : animState.Idle;
                }
                else
                {
                    aState = animState.Talking;
                }
                DoAnimations(Time.deltaTime);
                break;
        }        
    }
}
