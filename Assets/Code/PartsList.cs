﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct CryptidHeads {
    [SerializeField]
    public Sprite idle;
    [SerializeField]
    public Sprite[] walk;
}
[System.Serializable]
public struct CryptidHats {
    [SerializeField]
    public Sprite idle;
    [SerializeField]
    public Sprite[] walk;
}
[System.Serializable]
public struct CryptidBodys {
    [SerializeField]
    public Sprite idle;
    [SerializeField]
    public Sprite[] walk;
    [SerializeField]
    public Sprite[] interact;
}
[System.Serializable]
public struct CryptidLegs {
    [SerializeField]
    public Sprite idle;
    [SerializeField]
    public Sprite[] walk;

}
[System.Serializable]
public struct CryptidFaces {
    [SerializeField]
    public Sprite idle;
    [SerializeField]
    public Sprite[] walk;
    [SerializeField]
    public Sprite[] talk;
}
public class PartsList : MonoBehaviour {
    public static PartsList instance;
    private void Awake()
    {
        instance = this;
    }

    [SerializeField]
    public CryptidHeads[] heads;
    [SerializeField]
    public CryptidHats[] hats;
    [SerializeField]
    public CryptidBodys[] bodies;
    [SerializeField]
    public CryptidLegs[] legs;
    [SerializeField]
    public CryptidFaces[] faces;
}
