﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camfollow : MonoBehaviour {

    [SerializeField]
    Transform player;
    public Transform target;
    [SerializeField]
    float off_x, off_y, off_z;
	// Use this for initialization
	void Start () {
        target = player;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = new Vector3(target.position.x + off_x, target.position.y + off_y, target.position.z + off_z);
        transform.position = Vector3.Lerp(transform.position, pos, 0.1f);
	}

    public void reset_target() {
        target = player;
    }

    public void set_target(Transform t) {
        target = t;
    }

}
