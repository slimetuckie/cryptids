﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Plane_Tex_Scaler : MonoBehaviour {
    [Header("At this scale tiling is 1 x 1")]
    public Vector3 refTile;
    Vector3 lastScale;
    Material targetMat;

    public void Update() {
        if (transform.localScale != lastScale) {
            var t = transform.localScale;
            Vector2 endScale = new Vector2(t.x / refTile.x, t.z / refTile.z);

            if (targetMat == null)
                targetMat = GetComponent<MeshRenderer>().material;

            targetMat.SetVector("_Tiling", endScale);
        }
    }
}
