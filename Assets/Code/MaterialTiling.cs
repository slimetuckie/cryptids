﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MaterialTiling : MonoBehaviour {

    public Vector2 size = new Vector2(1, 1);
    public Material targetMat;

    public void Update() {
        targetMat.mainTextureScale = new Vector2(transform.localScale.x, transform.localScale.z) * size;
    }
}