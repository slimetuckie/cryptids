using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CryptidList {
    public CryptidData[] data;
}

[System.Serializable]
public class CryptidData {
    public string name, catchphrase;
    public int personality, fav_color, head_design, hat_design, leg_design, body_design, eye_design;
    public float location_y, location_x;
}


public class Database : MonoBehaviour {
    public bool ready = false;
    string download_url = "http://cryptidzones.gearhostpreview.com/download.php";
    [SerializeField]
    string json = "";

    public static Database db;
    public CryptidList cryptids = new CryptidList();
    

    private void Awake() {
        DontDestroyOnLoad(this.gameObject);
        if (db == null) {
            cryptids = new CryptidList();
            db = this;
        }
        else if(db != this)
        {
            DestroyImmediate(gameObject);
        }        
    }

    public void Start()
    {
        ready = false;
        DownloadCryptids();
    }

    public void Update() {
        ready = (json == "") ? false : true;
        if(ready) {

        }
    }

    IEnumerator SetCryptid(string url)  {
        WWW upload = new WWW(url);
        yield return upload;
        if (upload.error != null) {
            print("error: " + upload.error);
        }
    }

    IEnumerator GetCryptids() {
        WWW download = new WWW(download_url);
        yield return download;
        if (download.error != null){
            print("error: " + download.error);
        } else {
            json = download.text;
            CryptidData[] arr = JsonHelper.getJsonArray<CryptidData>(json);
            cryptids.data = arr;
        }
    }

    static string upload_url = "http://cryptidzones.gearhostpreview.com/upload.php?";
    static string upload_arguments;

    public static void UploadCryptid(CryptidData cryptid) {
        upload_arguments = "";
        upload_arguments += "name=" + cryptid.name;
        upload_arguments += "&catchphrase=" + cryptid.catchphrase;
        upload_arguments += "&personality=" + cryptid.personality;
        upload_arguments += "&fav_color=" + cryptid.fav_color;
        upload_arguments += "&head=" + cryptid.head_design;
        upload_arguments += "&body=" + cryptid.body_design;
        upload_arguments += "&leg=" + cryptid.leg_design;
        upload_arguments += "&hat=" + cryptid.hat_design;
        upload_arguments += "&eye=" + cryptid.eye_design;
        upload_arguments += "&gps_x=" + cryptid.location_x;
        upload_arguments += "&gps_y=" + cryptid.location_y;
        db.StartCoroutine("SetCryptid", upload_url + upload_arguments);
    }

    public static CryptidList DownloadCryptids() {
        db.json = "";
        db.StartCoroutine("GetCryptids");
        return (db.cryptids); // returns null since coroutine isn't done
    }

}
