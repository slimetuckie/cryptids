﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CryptidNPCLogic : InteractBase
{
    public CryptidData data;
    [SerializeField]
    CryptidPart[] BodyParts; //grody
    public enum animState
    {
        Idle, 
        Walking,
        Talking
    }
    public animState aState;

    public cryptidState State = cryptidState.unloaded;
    public enum cryptidState
    {
        unloaded,
        loaded
    }
    public override void OnEnable()
    {
        
    }
    public void initializeData(CryptidData d)
    {
        data = d;
        for (int i = 0; i < BodyParts.Length; i++) {
            BodyParts[i].Setup(this);
            BodyParts[i].FetchSprite();
        }
        State = cryptidState.loaded;
        interactables.Add(this);
    }
    public void doVisuals(CryptidData d)
    {
        data = d;
        for (int i = 0; i < BodyParts.Length; i++)
        {
            BodyParts[i].Setup(this);
            BodyParts[i].FetchSprite();
        }
        State = cryptidState.loaded;
    }

    public virtual void Update()
    {
        switch (State)
        {
            case cryptidState.loaded:
                DoAnimations(Time.deltaTime);
                break;
        }
    }

    animState last;
    public void DoAnimations(float dtime)
    {
        for (int i = 0; i < BodyParts.Length; i++)
        {
            BodyParts[i].CryptidUpdate(last != aState);
        }
        last = aState;
    }

    public override IEnumerator InteractWith()
    {
        aState = animState.Talking;
        DialogueController.instance.DialogueCamera.setDialogueTarget(transform);
        List<string> t = new List<string>();
        
        t.Add(data.catchphrase);
        Personality pers = CryptidDialogue.getPersonality(data.personality);
        string quip = pers.dialogue[UnityEngine.Random.Range(0, pers.dialogue.Length - 1)];
        t.Add(quip);
        AudioClip[] voice = CryptidDialogue.getPersonality(data.personality).voices;
        DialogueController.instance.TryDoText(t, data.name, delegate
        {
            aState = animState.Idle;
        }, voice);
        yield return null;

    }
}
