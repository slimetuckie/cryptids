﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CavePlacer : MonoBehaviour {

    public GameObject cave;
    public Vector3 offset;
    public Vector3 eulerRot;

    public int amount;

    [ContextMenu("Generate")]
    void gen()
    {
        Vector3 o = transform.position;
        for (int i = 0; i < amount; i++)
        {
            GameObject m = Instantiate(cave);
            m.transform.position = i * offset + o;
            m.transform.eulerAngles = eulerRot;
        }
    }
}
