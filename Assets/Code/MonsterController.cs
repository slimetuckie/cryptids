﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterController : MonoBehaviour {


}

public class monsterData
{
    //note from tuckie:
    //i kind of like the idea of this - but it will need a constructer that takes in a CryptidData and converts it into the vec2, enum etc.
    //we could potentially add those into my CryptidData - but they would need to be labelled as seperate fields. the json parser wouldn't
    //be able to set the data into an enum or vec2.
    //you can cast an int to an enum 
    // int k = 0; 
    // enumType castedEnum = (enumType)k
    
    public string name;
    public string catchPhrase;
    public m_Personality personality;
    public Color favoriteColor;

    public int head_design;
    public int hat_design;
    public int body_design;
    public int leg_design;
    public int eye_design;

    public Vector2 location;

    public monsterData(CryptidData data) {
        //set fields, data.name, new Vector2(data.location_x, data.location_y) etc.
    }

}

// in sql personality is just an int, so we should set dorky to 0 and so on so forth
public enum m_Personality
{
    Dorky,
    Swizzle,
    Clumpsy,
    Droopy,
    Wiggly
}