﻿Shader "Hidden/Wavy"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Speed ("Speed", Float) = 4 
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float4 _MainTex_TexelSize;
			float _Speed;

			fixed4 frag (v2f i) : SV_Target
			{
				float offset_x = ((_Speed * _Time) + i.uv.x) % _MainTex_TexelSize.w;
				float offset_y = ((_Speed * _Time) + i.uv.y) % _MainTex_TexelSize.w;
				float2 new_location = float2(offset_x, offset_y);
				
				fixed4 col = tex2D(_MainTex, new_location);
				return col;
			}
			ENDCG
		}
	}
}
