// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:9361,x:34623,y:31628,varname:node_9361,prsc:2|custl-3156-OUT,clip-9780-OUT;n:type:ShaderForge.SFN_LightVector,id:4972,x:32310,y:32026,varname:node_4972,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:659,x:32310,y:32171,prsc:2,pt:False;n:type:ShaderForge.SFN_Dot,id:8803,x:32612,y:32047,varname:node_8803,prsc:2,dt:1|A-4972-OUT,B-659-OUT;n:type:ShaderForge.SFN_Append,id:8351,x:32975,y:31914,varname:node_8351,prsc:2|A-1855-OUT,B-1855-OUT;n:type:ShaderForge.SFN_Tex2d,id:9502,x:33365,y:32064,ptovrint:False,ptlb:Texture,ptin:_MainTex,varname:node_8130,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:1,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1441,x:33360,y:31721,ptovrint:False,ptlb:Ramp,ptin:_Ramp,varname:node_7851,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a3b01920154ad5045afc05a59261d697,ntxv:0,isnm:False|UVIN-8351-OUT;n:type:ShaderForge.SFN_Multiply,id:5095,x:33781,y:31794,varname:node_5095,prsc:2|A-1441-RGB,B-9502-RGB;n:type:ShaderForge.SFN_LightAttenuation,id:42,x:32612,y:31903,varname:node_42,prsc:2;n:type:ShaderForge.SFN_SwitchProperty,id:3156,x:34148,y:31909,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_3156,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-5095-OUT,B-9239-OUT;n:type:ShaderForge.SFN_Tex2d,id:9814,x:33360,y:31528,ptovrint:False,ptlb:Gloss_ramp,ptin:_Gloss_ramp,varname:_Ramp_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a47ddb6c1f7efa444bdf8489986ca54b,ntxv:0,isnm:False|UVIN-8351-OUT;n:type:ShaderForge.SFN_Multiply,id:9239,x:33781,y:31932,varname:node_9239,prsc:2|A-9814-RGB,B-9502-RGB;n:type:ShaderForge.SFN_Multiply,id:1855,x:32792,y:31914,varname:node_1855,prsc:2|A-42-OUT,B-8803-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:9780,x:33934,y:32195,ptovrint:False,ptlb:Alpha Cutout,ptin:_AlphaCutout,varname:node_9780,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-3323-OUT,B-4487-OUT;n:type:ShaderForge.SFN_Vector1,id:3323,x:33365,y:32301,varname:node_3323,prsc:2,v1:1;n:type:ShaderForge.SFN_Power,id:4487,x:33781,y:32080,varname:node_4487,prsc:2|VAL-9502-A,EXP-2474-OUT;n:type:ShaderForge.SFN_Vector1,id:2474,x:33365,y:32231,varname:node_2474,prsc:2,v1:2;n:type:ShaderForge.SFN_TexCoord,id:6986,x:33365,y:32378,varname:node_6986,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_OneMinus,id:4695,x:33530,y:32388,varname:node_4695,prsc:2|IN-6986-U;n:type:ShaderForge.SFN_Power,id:9582,x:33709,y:32388,varname:node_9582,prsc:2|VAL-4695-OUT,EXP-5530-OUT;n:type:ShaderForge.SFN_Vector1,id:5530,x:33530,y:32523,varname:node_5530,prsc:2,v1:10;n:type:ShaderForge.SFN_OneMinus,id:7955,x:33885,y:32388,varname:node_7955,prsc:2|IN-9582-OUT;n:type:ShaderForge.SFN_Multiply,id:6675,x:34049,y:32388,varname:node_6675,prsc:2|A-7955-OUT,B-9189-OUT;n:type:ShaderForge.SFN_Normalize,id:9189,x:33885,y:32521,varname:node_9189,prsc:2|IN-9502-RGB;proporder:9502-1441-3156-9814-9780;pass:END;sub:END;*/

Shader "Shader Forge/Standard Lighting" {
    Properties {
        _MainTex ("Texture", 2D) = "gray" {}
        _Ramp ("Ramp", 2D) = "white" {}
        [MaterialToggle] _Gloss ("Gloss", Float ) = 0
        _Gloss_ramp ("Gloss_ramp", 2D) = "white" {}
        [MaterialToggle] _AlphaCutout ("Alpha Cutout", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"

            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Ramp; uniform float4 _Ramp_ST;
            uniform fixed _Gloss;
            uniform sampler2D _Gloss_ramp; uniform float4 _Gloss_ramp_ST;
            uniform fixed _AlphaCutout;

            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;               
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);

				o.uv0 = v.texcoord0;

                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
				
				float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                clip(lerp( 1.0, pow(_MainTex_var.a,2.0), _AlphaCutout ) - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

				UNITY_LIGHT_ATTENUATION(atten, i, i.posWorld.xyz)
                float shading = atten * (dot(lightDirection, i.normalDir));
				shading = max(0, shading);

				shading = max(1.0 / 255.0, min((254.0/255), shading)); 




				float2 node_8351 = float2(shading, 0);
                float4 _Ramp_var = tex2D(_Ramp,TRANSFORM_TEX(node_8351, _Ramp));
                float4 _Gloss_ramp_var = tex2D(_Gloss_ramp,TRANSFORM_TEX(node_8351, _Gloss_ramp));
                float3 finalColor = UNITY_LIGHTMODEL_AMBIENT + (_LightColor0.rgb) * lerp( (_Ramp_var.rgb*_MainTex_var.rgb), (_Gloss_ramp_var.rgb*_MainTex_var.rgb), _Gloss );
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            
			
			// Include shadowing support for point/spot
			#pragma multi_compile_fwdadd_fullshadows
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"


            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Ramp; uniform float4 _Ramp_ST;
            uniform fixed _Gloss;
            uniform sampler2D _Gloss_ramp; uniform float4 _Gloss_ramp_ST;
            uniform fixed _AlphaCutout;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
				
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );

				TRANSFER_SHADOW(o); 
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                clip(lerp( 1.0, pow(_MainTex_var.a,2.0), _AlphaCutout ) - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:

				UNITY_LIGHT_ATTENUATION(atten, i, i.posWorld.xyz)
                //float attenuation = LIGHT_ATTENUATION(i);
				float shading 
					= (atten*
						max(0, (dot(lightDirection, i.normalDir))));                

				shading = max(8.0/256.0, atten * (dot(lightDirection, i.normalDir)));
				shading = min((255.0/256), shading);


				float3 m = UNITY_LIGHTMODEL_AMBIENT;
                
                float2 node_8351 = float2(shading, 1);                
				
				float4 _Ramp_var = tex2D(_Ramp,TRANSFORM_TEX(node_8351, _Ramp));
                float4 _Gloss_ramp_var = tex2D(_Gloss_ramp,TRANSFORM_TEX(node_8351, _Gloss_ramp));
                
				float3 finalColor = (atten * _LightColor0.rgb) * lerp( (_Ramp_var.rgb*_MainTex_var.rgb), (_Gloss_ramp_var.rgb*_MainTex_var.rgb), _Gloss );                
				
				fixed4 finalRGBA = fixed4(finalColor * 1,0);


                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform fixed _AlphaCutout;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                clip(lerp( 1.0, pow(_MainTex_var.a,2.0), _AlphaCutout ) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
